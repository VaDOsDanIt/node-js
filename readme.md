**ДОКУМЕНТАЦИЯ**<br>

_1) Получение всех пользвателей:_<br>
    GET запрос на:
    `https://node-first-apps.herokuapp.com/getuserimg`.
    
_2) Добавление новой картинки:_ <br>
    POST запрос на:
    `https://node-first-apps.herokuapp.com/addimg`.
    <br>С параметрами `user` и `img`.
    
_3) Получение картинок отдельного юзера:_ <br>
    GET запрос на:
    `https://node-first-apps.herokuapp.com/finduser/:user`.
    <br>Где `:user` это имя нужного человека".
    
    
_4) Удаление картинки:_ <br>
    DEL запрос на:
    `https://node-first-apps.herokuapp.com/deleteimg/:id`.
    <br>Где `:id` это ID нужной картинки".
    