const ObjectID = require('mongodb').ObjectID;

module.exports = function (app, db) {
    app.post('/addimg', (req, res) => {
        const img = {user: req.body.user, img: req.body.url};
        db.collection('images').insertOne(img, (err, result) => {
            err ? res.send({"error": "Error"}) :
                res.send(result.ops[0]);
            console.log("\n|POST| Пришел запрос: ", req.body, '\n');
        });
    });

    app.get('/getuserimg', (req, res) => {
        db.collection('images').find({}, (err, result) => {
            err ? res.send({"ERROR": "ERROR"}) :
                result.toArray((err, items)=>{
                    res.send(items);
                    console.log("\n|GET| Пришел запрос на поиск всех элементов: ", '\n');
                })
        })
    })

    app.get('/finduser/:user', (req, res) => {
        const user = req.params.user;
        db.collection('images').find({"user": user}, (err, result) =>{
            err ? res.send({"ERROR": "ERROR"}) :
                result.toArray((err, items)=>{
                    res.send(items);
                })
            console.log("\n|GET| Пришел запрос на поиск всех элементов юзера: ", user, '\n');
        });
    })

    app.delete('/deleteimg/:id', (req, res) => {
        const id = req.params.id;
        db.collection('images').deleteOne({"_id": ObjectID(id)}, (err, result) => {
            err ? res.send({"ERROR": "ERROR"}) :
                res.send(result);
            console.log("\n|DEL| Пришел запрос на удаление юзера с айди: ", id, '\n');

        })
    })

};