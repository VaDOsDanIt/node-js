const express = require("express");
const MongoClient = require('mongodb');
const bodyParser = require('body-parser');
const db = require('./configDB');
const cors = require('cors');
const app = express();

const port = process.env.PORT || 8000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
MongoClient.connect(process.env.MONGODB_URI || db.url, {useUnifiedTopology: true}, (err, database) => {
    if (err) return console.log(err);

    const db = database.db("img-api");
    require('./req/request')(app, db);
    app.listen(port, () => {
        console.clear();
        console.log("Мы открыты на порту: " + port);
    });
});